# frozen_string_literal: true

require_relative './config/environment'

raise 'Migrations are pending. Run `rake db:migrate` to resolve the issue.' if ActiveRecord::Base.connection.migration_context.needs_migration?

if ENV['LOCAL_TESTING'].nil?
  Raven.configure do |config|
    config.dsn = "https://#{ENV['SENTRY_DSN']}@sentry.gitlab.net/#{ENV['SENTRY_PROJECT_ID']}"
  end
  use Raven::Rack
end

run App
