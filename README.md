# Nominator bot

This is a slack bot created to use within GitLab to nominate team members for discretionary bonuses. The way it can be used is

```shell
/nominate @someone
```

Once submitted, the app will connect to our HR system (BambooHR) and find the team member.

![Image of Slack Dialog](/docs/images/example.png)

Once the user submits the dialog, two things will happen:

- the nomination is added to the database
- the approval flow is kicked off

![Image of manager](/docs/images/example_2.png)

## Approval flow

```mermaid
graph TD;
  A[Nomination] -->|Bot logs and sends to manager| C;
  C{Manager}
  C -->|Reject| D[Bot logs];
  C -->|Approve| F;
  F{Manager's Leader}
  F -->|Reject| H[Bot logs];
  F -->|Approve| L;
  L{People Group}
  L -->|Reject| M[Bot logs];
  L -->|Approve| N[Bot logs and sends to BambooHR];
```

## Development

### Initial setup

- Clone the project `git@gitlab.com:gitlab-com/people-group/peopleops-eng/nominatorbot.git`
- cd `nominatorbot`
- `bundle install`
- Set up a PostgreSQL database. For a quick ephermal database, you can use a container:

    ```shell
    docker run -p 5432:5432 -e POSTGRES_PASSWORD=nominatorbot --rm -it postgres:${POSTGRES_VERSION}
    export DATABASE_URL=postgres://postgres:nominatorbot@localhost:5432
    ```

    where `$POSTGRES_VERSION` is the same as in `.gitlab-ci.yml`.

- copy the `.env.example` into an `.env` file.
- `rake db:create db:migrate`
- `rake db:create db:migrate RACK_ENV='test'`

At this point you should be able to run the test suite:

```shell
bundle exec rspec
```

To run the application, keep going:

- run the app `bundle exec rackup -p 4567`
- next you need a tool like [ngrok](https://ngrok.com/) to expose your localhost publicly (`./ngrok http 4567`)

You will have to do that last step, otherwise you can't test the connection with Slack itself and
the functionalities will be very limited.

Add the email linked to your account in the `TEST_EMAIL` env variable.

### Slack setup

Set up a Slack application in a Slack workspace. People Engineering at GitLab uses a seperate Slack
workspace to play around with our custom bots. Ask any of them to be invited. You can set up a Slack
application [here](https://api.slack.com/apps). Click the `Create New App` button. It will ask you the
name and which workspace you want to set up the app.

Once created, you have access to the `verification token` and `signing secret`. Add these to the relevant
variables in the `.env` file.

Navigate to `Slash commands` for your Slack application and click the `Create New Command` button. Fill in
the following:

- Command: `/nominate`
- Request URL: `http://FILL_IN.ngrok.io/nominate` (assuming you're using ngrok - otherwise just the URL that is exposing your localhost + `/nominate`)
- Description: (just something as it can't be empty)

Click save.

Navigate to `Interactivity & Shortcuts` and toggle the button. This will reveal a form. In the Request URL, fill
in the following: `http://FILL_IN.ngrok.io/nominate_submit`

Navigate to `OAuth & Permissions`. Add the following scopes to the `Bot Token Scopes`:

- `app_mentions:read`
- `chat:write`
- `commands`
- `im:write`
- `users:read`
- `users:read:email`

Once done, scroll up on the page and click the `Install to Workspace` button. Once you've done this and you're
redirected to the settings, you'll see a `Bot User OAuth Access Token`. Copy this into the `SLACK_API_TOKEN` env
variable.

You will need one Slack channel IDs. If you're using the existing workspace, ask someone in the team for the IDs.
Otherwise create a slack channels and add it to the `PEOPLE_GROUP_CHANNEL`.
