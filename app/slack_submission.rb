# frozen_string_literal: true

# The purpose of this class is to transform the Slack submission
# payload in an object.

class SlackSubmission
  include Connectors

  attr_reader :nomination

  def initialize(submission)
    @submission = submission
  end

  def as_nomination_params
    {
      nominee: nominee_name,
      nominated_by: { slack_name: nominator, slack_id: nominator_slack_id },
      values: values,
      reasoning: reasoning,
      criteria: criteria,
      nominee_employee_number: nominee_employee_number
    }
  end

  private

  def nominator_slack_id
    @submission.dig('user', 'id')
  end

  def nominator
    @submission.dig('user', 'name')
  end

  def selected_user
    @selected_user ||= fields.dig('nominee', 'nominee', 'selected_conversation')
  end

  def slack_user
    slack_client.find_user_by_id(selected_user)
  end

  def bamboo_team_member
    @bamboo_team_member ||= bamboo_client.search_team_member_by_email!(slack_user.user.profile.email)
  end

  def nominee_name
    return "#{bamboo_team_member['preferredName'] || bamboo_team_member['firstName']} #{bamboo_team_member['customPreferredLastName'] || bamboo_team_member['lastName']}" if bamboo_team_member

    # The only time we should get into this part of the code is:
    # - when the team member's work email on BambooHR != email on Slack
    # - AND they have not been added to the mapper file
    Interface::Sentry.send_error("Can't find the team member for #{selected_user}, email: #{slack_user.user.profile.email}")
    selected_user
  end

  def nominee_employee_number
    bamboo_team_member&.dig('employeeNumber') || 0
  end

  def reasoning
    fields.dig('motivation', 'motivation', 'value')
  end

  def criteria
    fields.dig('criteria', 'criteria', 'value')
  end

  def values
    fields.dig('values', 'values', 'selected_options').map { |option| option['value'] }
  end

  def fields
    @submission.dig('view', 'state', 'values')
  end
end
