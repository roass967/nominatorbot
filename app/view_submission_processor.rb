# frozen_string_literal: true

# This class handles all the incoming submissions from Slack with the
# type `view_sumission`. In practice this means, every action triggers
# from a modal. All this class does is, reroute to the correct services
# to handle the incoming submission correctly.

# Currently the application has two modals:
# - submit a new nomination
# - update an existing nomination

class ViewSubmissionProcessor
  def run(submission)
    submission_type = submission.dig('view', 'callback_id')
    case submission_type
    when 'submit_nomination'
      CreateNomination.call(submission: submission)
    when 'submit_rejection'
      UpdateDecision.call(submission: submission)
    else
      UpdateNomination.call(submission: submission)
    end
  end
end
