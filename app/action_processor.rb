# frozen_string_literal: true

class ActionProcessor
  include Connectors

  def run(action)
    action_type = action['actions'].first['value']

    case action_type
    when 'my_nominations'
      HomeView::UserNominations.call(trigger_id: action['trigger_id'], user_id: action['user']['id'])
    when 'my_reviews'
      HomeView::ReviewedNominations.call(trigger_id: action['trigger_id'], user_id: action['user']['id'])
    when 'nominate_tm'
      Modal::Nomination.call(trigger_id: action['trigger_id'])
    when /^reminder-/
      Review::Retrigger.call(nomination_id: action_type, user: action['user'])
    end
  end
end
