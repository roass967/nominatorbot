# frozen_string_literal: true

# This class is responsible for asking the manager for a review of a nomination

module Review
  class Manager < ApplicationService
    include Helpers

    def initialize(nomination:)
      @nomination = nomination
    end

    def call
      prompt_manager_for_review(@nomination)
    rescue StandardError => e
      Interface::Sentry.send_error("Can't find manager for nomination #{@nomination.id}, error #{e}", @nomination)
    end
  end
end
