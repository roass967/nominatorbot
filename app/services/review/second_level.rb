# frozen_string_literal: true

# This class is responsible for asking the second level manager for a review of a nomination

module Review
  class SecondLevel < ApplicationService
    include Helpers

    def initialize(nomination:)
      @nomination = nomination
    end

    def call
      prompt_manager_for_review(@nomination, second_level: true)
    rescue StandardError => e
      Interface::Sentry.send_error("Second level manager can not be found for #{@nomination.id}, error #{e}", @nomination)
    end
  end
end
