# frozen_string_literal: true

# This class is responsible for fetching all the nominations that the
# user has reviewed and showing them in the home view tab of the Slack app.

module HomeView
  class ReviewedNominations < ApplicationService
    include Helpers

    def initialize(trigger_id:, user_id:)
      @trigger_id = trigger_id
      @user_id = user_id
    end

    def call
      nominations = Nomination.reviewed_by(@user_id)

      if nominations.none?
        home_view = home_view_template.sub('`__NOMINATIONS__`', no_reviewed_nominations)
      else
        parsed_nominations = parse_nominations(nominations, include_nominated_by: true)
        home_view = home_view_template.sub('`__NOMINATIONS__`', parsed_nominations.join(','))
      end

      slack_client.publish_view(user_id: @user_id, trigger: @trigger_id, view: home_view)
    end
  end
end
