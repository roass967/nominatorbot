# frozen_string_literal: true

class UpdateNomination < ApplicationService
  include Review::Helpers

  REGEX = /channel-(?<channel>.*)-ts-(?<timestamp>.*)/.freeze

  def initialize(submission:)
    @submission = submission
  end

  def call
    nomination = Nomination.find(@submission.dig('view', 'private_metadata'))
    nomination.update!(reasoning: reasoning, criteria: criteria)
    matches = callback_id.match(REGEX)

    nomination_message = basic_message(nomination)
    attachments = message_attachments(nomination.id).to_json.gsub!('`__REVIEW_STAGE_NAME__`', review_stage_name(nomination.status))

    slack_client.update_message(channel: matches[:channel], timestamp: matches[:timestamp], text: nomination_message, attachments: attachments)
  end

  private

  def review_stage_name(status)
    case status
    when 'submitted'
      'submission_approval'
    when 'manager_approved'
      'second_level_review'
    when 'second_level_approved'
      'total_rewards_approval'
    end
  end

  def callback_id
    @submission.dig('view', 'callback_id')
  end

  def reasoning
    fields.dig('motivation', 'motivation', 'value')
  end

  def criteria
    fields.dig('criteria', 'criteria', 'value')
  end

  def fields
    @submission.dig('view', 'state', 'values')
  end
end
