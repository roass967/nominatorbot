# frozen_string_literal: true

class UpdateDecision < ApplicationService
  def initialize(submission:)
    @submission = submission
  end

  def call
    nomination = Nomination.find(@submission.dig('view', 'private_metadata'))
    decision = nomination.decisions.made_by(submitted_by).last
    decision.rejection_reason = selected_reason
    decision.save
  end

  private

  def submitted_by
    @submission.dig('user', 'id')
  end

  def selected_reason
    @submission.dig('view', 'state', 'values', 'reasons', 'rejection-reason', 'selected_option', 'value')
  end
end
