# frozen_string_literal: true

class ApplicationService
  include Connectors

  def self.call(*args, **kwargs, &block)
    new(*args, **kwargs, &block).call
  end
end
