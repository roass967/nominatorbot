# frozen_string_literal: true

# The only responsibility of this service is to send the
# rejection modal to whenever a nomination has been rejected.

module Modal
  class Reject < ApplicationService
    def initialize(trigger_id:, nomination:)
      @trigger_id = trigger_id
      @nomination = nomination
    end

    def call
      view = view_json
      new_view = view
        .sub('`__NOMINATION_ID__`', @nomination.id)

      slack_client.send_modal_message(trigger: @trigger_id, view: new_view)
    end

    private

    def view_json
      '{
        "type": "modal",
        "title": {
          "type": "plain_text",
          "text": "Discretionary Bonus",
          "emoji": true
        },
        "submit": {
          "type": "plain_text",
          "text": "Add reject reason",
          "emoji": true
        },
        "close": {
          "type": "plain_text",
          "text": "Cancel",
          "emoji": true
        },
        "blocks": [
          {
            "block_id": "reasons",
            "type": "input",
            "element": {
              "type": "radio_buttons",
              "options": [
                {
                  "text": {
                    "type": "plain_text",
                    "text": "Invalid Criteria",
                    "emoji": true
                  },
                  "value": "invalid_criteria"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": "Duplicate Information",
                    "emoji": true
                  },
                  "value": "duplicate"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": "Insufficient Content",
                    "emoji": true
                  },
                  "value": "insufficient"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": "Performance Concern",
                    "emoji": true
                  },
                  "value": "performance"
                },
                {
                  "text": {
                    "type": "plain_text",
                    "text": "Offboarded/Departing (team member not on the payroll anymore)",
                    "emoji": true
                  },
                  "value": "offboarded"
                }
              ],
              "action_id": "rejection-reason"
            },
            "label": {
              "type": "plain_text",
              "text": "Please select a reason for the nomination rejection:",
              "emoji": true
            }
          }
        ],
        "callback_id": "submit_rejection",
        "private_metadata": "`__NOMINATION_ID__`"
      }'
    end
  end
end
