# frozen_string_literal: true

class Decision < ActiveRecord::Base
  belongs_to :nomination

  validates :made_by, presence: true
  validates :status, presence: true
  validates :nomination, presence: true

  VALID_STATES = %w[approved rejected].freeze
  enum status: VALID_STATES

  VALID_REJECTION_REASONS = %w[invalid_criteria duplicate insufficient performance offboarded].freeze
  enum rejection_reason: VALID_REJECTION_REASONS
  REJECTION_REASON_MAPPING = {
    'invalid_criteria' => 'Invalid Criteria',
    'duplicate' => 'Duplicate Information',
    'insufficient' => 'Insufficient Content',
    'performance' => 'Performance Concern',
    'offboarded' => 'Offboarded/Departing (team member not on the payroll anymore)'
  }.freeze

  scope :made_by, ->(slack_id) { where("made_by->>'slack_id' = ?", slack_id) }
end
