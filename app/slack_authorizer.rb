# frozen_string_literal: true

class SlackAuthorizer
  UNAUTHORIZED_MESSAGE = 'Oops! Looks like the application is not authorized! Please review the token configuration.'
  UNAUTHORIZED_RESPONSE = ['200', { 'Content-Type' => 'text' }, [UNAUTHORIZED_MESSAGE]].freeze
  NOT_FOUND_POST_RESPONSE = ['404', { 'Content-Type' => 'text' }, ['This route does not exist']].freeze
  ALLOWED_POST_CALLS = ['/nominate', '/nominate_submit', '/events'].freeze

  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)

    if req.post? && !allowed_post_call?(req)
      NOT_FOUND_POST_RESPONSE
    elsif req.get? || valid_events_call?(req) || (valid_token?(req.params) && valid_signature?(req))
      @app.call(env)
    else
      UNAUTHORIZED_RESPONSE
    end
  end

  private

  def allowed_post_call?(req)
    ALLOWED_POST_CALLS.include?(req.env['PATH_INFO'])
  end

  def valid_token?(params)
    token_from_params(params) === ENV['SLACK_VERIFICATION_TOKEN']
  end

  def valid_signature?(req)
    timestamp = req.env['HTTP_X_SLACK_REQUEST_TIMESTAMP']
    signature = req.env['HTTP_X_SLACK_SIGNATURE']
    body = req.body.read
    hex_hash = OpenSSL::HMAC.hexdigest(digest, ENV['SLACK_SIGNING_SECRET'], ['v0', timestamp, body].join(':'))
    computed_signature = ['v0', hex_hash].join('=')
    computed_signature == signature
  end

  def valid_events_call?(req)
    # Token is not sent with the events call
    req.env['REQUEST_URI'].end_with?('/events') && valid_signature?(req)
  end

  def digest
    OpenSSL::Digest.new('SHA256')
  end

  def token_from_params(params)
    return params['token'] unless params['payload'].present?

    params = JSON.parse(params['payload'])
    params['token']
  end
end
