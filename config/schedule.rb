# frozen_string_literal: true

# Learn more: http://github.com/javan/whenever

set :environment, ENV['RACK_ENV']
set :output, "log/cron.log"

every 1.day do
  rake "retrigger_idle_nominations:run"
end
