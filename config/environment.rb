# frozen_string_literal: true
require 'raven'
require 'require_all'
require 'dotenv/load'
require 'active_support/core_ext/hash/reverse_merge' # workaround until https://github.com/rails/rails/pull/44230 is merged
require 'sinatra'
require 'sinatra/activerecord'
require 'peoplegroup/connectors'

ENV['RACK_ENV'] ||= "development"

# Requires all gems in Gemfile and app directory
require 'bundler/setup'
Bundler.require(:default, ENV['RACK_ENV'])

require "sinatra/reloader" if development?
require 'byebug' if development?

require './app/connectors'
require_all './app'
require './app'
