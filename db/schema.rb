# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_02_22_155656) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "decisions", force: :cascade do |t|
    t.json "made_by", null: false
    t.integer "status", null: false
    t.uuid "nomination_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "rejection_reason"
    t.index ["nomination_id"], name: "index_decisions_on_nomination_id"
  end

  create_table "nominations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "nominee", null: false
    t.json "nominated_by", null: false
    t.string "values", null: false, array: true
    t.text "reasoning", null: false
    t.integer "status", default: 0, null: false
    t.datetime "synced_to_bamboo_at"
    t.string "slack_ts"
    t.integer "spreadsheet_row_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "retriggered_at"
    t.text "criteria"
    t.integer "nominee_employee_number"
    t.integer "current_approver_employee_number"
    t.index ["nominee_employee_number"], name: "index_nominations_on_nominee_employee_number"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "decisions", "nominations"
end
