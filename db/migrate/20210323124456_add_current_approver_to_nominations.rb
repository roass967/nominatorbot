# frozen_string_literal: true

class AddCurrentApproverToNominations < ActiveRecord::Migration[6.0]
  def change
    add_column :nominations, :current_approver_bamboo_id, :integer
  end
end
