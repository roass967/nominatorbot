# frozen_string_literal: true
class AddNomineeEmployeeIdToNominations < ActiveRecord::Migration[7.0]
  def change
    # # Add the :nominee_employee_number to the :nominations table
    add_column :nominations, :nominee_employee_number, :integer
  end
end
