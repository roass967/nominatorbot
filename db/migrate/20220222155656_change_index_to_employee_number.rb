# frozen_string_literal: true

class ChangeIndexToEmployeeNumber < ActiveRecord::Migration[7.0]
  def change
    # Change the index to use employee_number
    remove_index :nominations, :nominee_bamboo_id
    add_index :nominations, :nominee_employee_number

    # Remove old bamboo_id from columns
    remove_column :nominations, :nominee_bamboo_id, :integer
    remove_column :nominations, :current_approver_bamboo_id, :integer
  end
end
