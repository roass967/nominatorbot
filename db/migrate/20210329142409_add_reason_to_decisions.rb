# frozen_string_literal: true

class AddReasonToDecisions < ActiveRecord::Migration[6.0]
  def change
    add_column :decisions, :rejection_reason, :integer
  end
end
