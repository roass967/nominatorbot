# frozen_string_literal: true

class AddDefaultValueForStatus < ActiveRecord::Migration[6.0]
  def change
    change_column_default :nominations, :status, from: nil, to: 0
  end
end
