# frozen_string_literal: true

class AddCriteriaMeetToNominations < ActiveRecord::Migration[6.0]
  def change
    add_column :nominations, :criteria, :text
  end
end
