# frozen_string_literal: true
class AddCurrentApproverEmployeeIdToNominations < ActiveRecord::Migration[7.0]
  def change
    # Add the :current_approver_employee_number to the :nominations table
    add_column :nominations, :current_approver_employee_number, :integer
  end
end
