# frozen_string_literal: true

class App < Sinatra::Base
  use SlackAuthorizer

  configure :development do
    register Sinatra::Reloader
  end

  configure :production, :development do
    enable :logging

    before { logger.info "Received: #{params}" }
  end

  HELP_RESPONSE = <<~MSG
    Use `/nominate` to nominate a team member for a discretionary bonus. Just type `/nominate`
    and a modal will open where you can fill in everything about the nomination.
  MSG

  MISSING_FEATURE_RESPONSE = <<~MSG
    Please use the `/nominate` command without any extra value. Example: `/nominate`.
    You will be able to select the nominee in the form.
  MSG

  error 404 do
    content_type :json
    { error: { code: 404, message: "This route does not exist" } }.to_json
  end

  get '/' do
    status 200
  end

  get '/health' do
    'hello it works'
  end

  post '/nominate' do
    nominee = params['text']
    logger.info "Nominee: #{nominee}"

    if nominee == 'help'
      HELP_RESPONSE
    elsif nominee.present?
      MISSING_FEATURE_RESPONSE
    else
      Modal::Nomination.call(trigger_id: params['trigger_id'])
      status 200
    end

  rescue StandardError => e
    Interface::Sentry.send_error(e)
  end

  post '/nominate_submit' do
    submission = JSON.parse(params['payload'])
    submission_type = submission['type']
    logger.info "Interactivity type: #{submission['type']}"
    logger.info "Submission: #{submission}"

    Thread.new do
      status 200
      body ''
    end

    Thread.new do
      case submission_type
      when 'view_submission'
        begin
          ViewSubmissionProcessor.new.run(submission)
        rescue StandardError => e
          Interface::Sentry.send_error("Error #{e} for submission: #{submission}")
        end
      when 'block_actions'
        ActionProcessor.new.run(submission)
      else
        begin
          NominationProcessor.new.process_review(submission)
        rescue StandardError => e
          Interface::Sentry.send_error("Error #{e} for submission: #{submission['callback_id']}")
        end
      end
    end
  end

  post '/events' do
    request.body.rewind # because the body was already read

    event = JSON.parse(request.body.read)
    logger.info "event: #{event}"

    EventProcessor.new.run(event['event']) if event['event']
    event['challenge'] unless event['challenge'].nil?
  end
end
