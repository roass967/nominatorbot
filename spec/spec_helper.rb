# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
require 'rack/test'

module RSpecMixin
  include Rack::Test::Methods
  def app
    Sinatra::Application
  end

  def load_fixture(file_name)
    JSON.parse(File.read(File.join(__dir__, "/fixtures/#{file_name}.json")))
  end
end

require 'simplecov'

SimpleCov.start do
  add_filter 'vendor'
  add_filter 'bundle'
  add_filter 'spec'

  add_group 'Services', 'app/services'
  add_group 'Models', 'app/models'
  add_group 'Interfaces', 'app/interface'
end

require_relative '../config/environment'

# For RSpec 2.x and 3.x
RSpec.configure do |config|
  config.include RSpecMixin

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  Encoding.default_external = 'UTF-8'

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  # FactoryBot setup
  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end
end
