# frozen_string_literal: true

require_relative '../spec_helper'

RSpec.describe Decision, type: :model do
  let(:decision) { build(:decision) }

  it "has a valid factory" do
    expect(create(:decision)).to be_valid
  end

  describe 'validations' do
    it 'is not valid without a made by' do
      decision.made_by = nil
      expect(decision).not_to be_valid
    end

    it 'is not valid without a status' do
      decision.status = nil
      expect(decision).not_to be_valid
    end

    it 'is not valid without a nomination' do
      decision.nomination = nil
      expect(decision).not_to be_valid
    end
  end
end
