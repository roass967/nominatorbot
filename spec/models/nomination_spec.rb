# frozen_string_literal: true

require_relative '../spec_helper'

RSpec.describe Nomination, type: :model do
  let(:nomination) { build(:nomination) }

  it "has a valid factory" do
    expect(create(:nomination)).to be_valid
  end

  describe 'validations' do
    it 'is not valid without a nominee' do
      nomination.nominee = nil
      expect(nomination).not_to be_valid
    end

    it 'is not valid without a nominated by' do
      nomination.nominated_by = nil
      expect(nomination).not_to be_valid
    end

    it 'is not valid without values' do
      nomination.values = nil
      expect(nomination).not_to be_valid
    end

    it 'is not valid without a reasoning' do
      nomination.reasoning = nil
      expect(nomination).not_to be_valid
    end

    it 'is not valid without a status' do
      nomination.status = nil
      expect(nomination).not_to be_valid
    end
  end

  describe '#values_in_emojis' do
    it 'returns the values mapped to emojis' do
      expect(nomination.values_in_emojis).to eq(":iteration-tanuki:")
    end
  end

  describe '#rejected?' do
    it 'returns false when it is not in the REJECTED_STATES' do
      expect(nomination.rejected?).to eq(false)
    end

    it 'returns true when it is in the REJECTED_STATES' do
      nomination.status = 'total_rewards_rejected'
      expect(nomination.rejected?).to eq(true)
    end
  end

  describe '#approved?' do
    it 'returns false when it is not in the FULLY_APPROVED_STATES' do
      expect(nomination.approved?).to eq(false)
    end

    it 'returns true when it is in the FULLY_APPROVED_STATES' do
      nomination.status = 'total_rewards_approved'
      expect(nomination.approved?).to eq(true)
    end
  end

  describe '#can_be_retriggered' do
    it 'is false when the nomination was just created' do
      nomination.created_at = DateTime.now
      expect(nomination.can_be_retriggered?).to eq(false)
    end

    it 'is true when the nomination was created 24 hours ago' do
      nomination.created_at = DateTime.now - 26.hours
      expect(nomination.can_be_retriggered?).to eq(true)
    end

    it 'is false when the nomination status is rejected' do
      nomination.created_at = DateTime.now - 26.hours
      nomination.status = 'manager_rejected'
      expect(nomination.can_be_retriggered?).to eq(false)
    end

    it 'is false when the nomination is fully approved' do
      nomination.created_at = DateTime.now - 26.hours
      nomination.status = 'total_rewards_approved'
      expect(nomination.can_be_retriggered?).to eq(false)
    end

    it 'is true when the last decision was more than 24 hours ago' do
      nomination.created_at = DateTime.now - 26.hours
      create(:decision, nomination: nomination, created_at: DateTime.now - 26.hours)
      expect(nomination.can_be_retriggered?).to eq(true)
    end

    it 'is false when the last decision was less than 24 hours ago' do
      nomination.created_at = DateTime.now - 26.hours
      create(:decision, nomination: nomination, created_at: DateTime.now - 2.hours)
      expect(nomination.can_be_retriggered?).to eq(false)
    end

    it 'is false when the nomination was retriggered less than 24 hours ago' do
      nomination.created_at = DateTime.now - 26.hours
      nomination.retriggered_at = DateTime.now - 2.hours
      expect(nomination.can_be_retriggered?).to eq(false)
    end
  end

  describe '#sync_to_bamboo' do
    let(:bamboo_mock) { instance_double(PeopleGroup::Connectors::Bamboo, add_bonus: true) }
    let(:team_members_file) { load_fixture 'team_members' }

    before do
      allow(ENV).to receive(:[]).with('LOCAL_TESTING').and_return(false)

      allow(PeopleGroup::Connectors::Bamboo).to receive(:new).and_return(bamboo_mock)
      allow(bamboo_mock).to receive(:search_team_member).and_return(team_members_file[4])
    end

    it 'will not continue if the nomination is not approved by TR' do
      nomination.sync_to_bamboo
      expect(bamboo_mock).not_to have_received(:add_bonus)
    end

    context 'when approved' do
      before do
        nomination.update(status: 'total_rewards_approved')
        nomination.sync_to_bamboo
      end

      it 'syncs to BambooHR' do
        expect(bamboo_mock).to have_received(:add_bonus).once
      end

      it 'updates the nomination' do
        nomination.reload
        expect(nomination.status).to eq('synced')
      end
    end
  end
end
