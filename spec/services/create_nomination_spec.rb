# frozen_string_literal: true

require_relative '../spec_helper'

describe CreateNomination do
  let(:team_members_file) { load_fixture 'team_members' }
  let(:nomination_submission) { load_fixture 'nomination_submission' }
  let(:slack_mock) { instance_double(PeopleGroup::Connectors::Slack, send_message: double(message: double(ts: '234567')), update_message: true) }
  let(:slack_user) { double(user: double(id: 'something', profile: double(email: 'something@something.com'))) }
  let(:bamboo_mock) { instance_double(PeopleGroup::Connectors::Bamboo, add_bonus: true) }

  let(:nomination_object) do
    nomination = Nomination.create(
      nominee: 'Lien Van Den Steen',
      nominated_by: { slack_id: 'UTPD9QUJ0', slack_name: 'lvandensteen' },
      values: %w[collaboration dib],
      status: :manager_approved,
      reasoning: 'She is awesome',
      nominee_employee_number: 5
    )
    nomination
  end

  let(:review_attachments) do
    [
      {
        text: 'Choose if you want to approve or reject this nomination',
        fallback: 'You are unable to approve or reject the nomination through Slack, you will have to do it manually.',
        callback_id: "uuid-#{nomination_object.id}",
        color: '#3AA3E3',
        attachment_type: 'default',
        actions: [
          {
            name: 'submission_approval',
            text: 'Approve',
            type: 'button',
            value: 'approve',
            "style": "primary"
          },
          {
            name: 'submission_approval',
            text: 'Reject',
            type: 'button',
            value: 'reject',
            "style": "danger"
          },
          {
            name: 'submission_approval',
            text: 'Update',
            type: 'button',
            value: 'update'
          }
        ]
      }
    ].to_json
  end

  let(:review_text) do
    nomination_message = <<~MSG
      <@lvandensteen> nominated Lien Van Den Steen for a discretionary bonus for the values: :collaboration-tanuki:, :diversity-tanuki:.
      They gave the following reasoning for the nomination:

      ```She is awesome```

      You can read more about the process in our handbook: https://about.gitlab.com/handbook/incentives/#nominator-bot-process
      Optionally, if you want to talk more in-depth about this nomination, please schedule a meeting with <@lvandensteen>.
    MSG
    nomination_message
  end

  before do
    allow(PeopleGroup::Connectors::Slack).to receive(:new).and_return(slack_mock)
    allow(slack_mock).to receive(:bamboo_email_lookup).with('nadia@flitglab.com').and_return({ 'user': { 'id': "1234", 'name': 'Nadia' } })

    allow(PeopleGroup::Connectors::Bamboo).to receive(:new).and_return(bamboo_mock)
    allow(bamboo_mock).to receive(:search_employee_by_field).with(field: 'employeeNumber', value: 5).and_return(team_members_file[4])
    allow(bamboo_mock).to receive(:fetch_manager).with(team_members_file[4]).and_return(team_members_file[5])
    allow(bamboo_mock).to receive(:search_team_member_by_email!).and_return(team_members_file[4])

    allow(slack_mock).to receive(:find_user_by_id).with('UTPD9QUJ0').and_return(slack_user)

    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('LOCAL_TESTING').and_return(false)
  end

  describe '#call' do
    before do
      allow(Nomination).to receive(:create!).and_return(nomination_object)
      described_class.call(submission: nomination_submission)
    end

    it 'creates a nomination' do
      expect(Nomination).to have_received(:create!).once
    end

    it 'thanks the nominator' do
      expect(slack_mock).to have_received(:send_message).with(channel: 'UTPD9QUJ0', text: "Thanks for nominating Lien Van Den Steen for the values: :collaboration-tanuki:, :diversity-tanuki: because:\n\n```She is awesome```\n\nYou added this as how the criteria are met:\n\n``````\n").once
    end

    it 'finds the manager on Slack through Bamboo data' do
      expect(bamboo_mock).to have_received(:fetch_manager).with(team_members_file[4]).exactly(2).times
      expect(slack_mock).to have_received(:bamboo_email_lookup).with('nadia@flitglab.com').exactly(1)
    end

    it 'asks the manager for review' do
      expect(slack_mock).to have_received(:send_message).with(channel: nil, text: review_text, attachments: review_attachments).once
    end

    it 'sets the slack_ts on the nomination' do
      expect(nomination_object.slack_ts).not_to be_nil
    end

    it 'sets the current_approver_employee_number on the nomination' do
      expect(nomination_object.current_approver_employee_number).to eq(6)
    end
  end
end
