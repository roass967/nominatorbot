# frozen_string_literal: true

FactoryBot.define do
  factory :nomination do
    nominee { 'Jane Doe' }
    nominated_by { { slack_name: 'John', slack_id: 'XTR567' } }
    values { ['iteration'] }
    reasoning { 'Because Jane is a great iterator' }
    status { 0 }
    slack_ts { '25639.28922' }
    nominee_employee_number { 5 }
  end
end
