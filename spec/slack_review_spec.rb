# frozen_string_literal: true

require_relative 'spec_helper'

describe SlackReview do
  let(:review_submission) { load_fixture 'review_submission' }

  subject { described_class.new(review_submission) }

  describe '#reviewer' do
    it 'returns the user details' do
      expect(subject.reviewer).to eq({ "id" => "UTPD9QUJ0", "name" => "lvandensteen" })
    end
  end

  describe '#status' do
    it 'parses the status correctly' do
      expect(subject.status).to eq(:manager_approved)
    end
  end

  describe '#needs_update' do
    it 'can inform when an update is needed' do
      expect(subject.needs_update?).to eq(false)
    end
  end

  describe '#decision_status' do
    it 'returns what the decision was' do
      expect(subject.decision_status).to eq(:approved)
    end
  end
end
