# frozen_string_literal: true
namespace :switch_to_employee_number do
  task dry_run: :environment do
    count = 0
    Nomination.where(nominee_employee_number: nil).find_each do |nomination|
      pp update(nomination)
      count += 1
    end

    p "DRY_RUN: Migrated #{count} team members to use employeeNumber."
  end

  task run: :environment do
    count = 0
    Nomination.where(nominee_employee_number: nil).find_each do |nomination|
      update(nomination).save
      count += 1
    end

    p "Migrated #{count} team members to use employeeNumber."
  end
end

def client
  @client ||= PeopleGroup::Connectors::Bamboo.new
end

def approver(nomination)
  # Return nil if there is no approver
  return nil unless nomination.current_approver_bamboo_id

  # Return the employeeNumber of the current_approver
  client.get_employee_details(nomination.current_approver_bamboo_id)['employeeNumber']
end

def update(nomination)
  # Get the nominee's employeeNumber
  nominee = client.get_employee_details(nomination.nominee_bamboo_id)

  raise "We could not find the team member #{nomination.id} by their bamboo_id." unless nominee

  nomination.nominee_employee_number = nominee['employeeNumber']
  nomination.current_approver_employee_number = approver(nomination)

  # Return our UNSAVED changes
  nomination
end
