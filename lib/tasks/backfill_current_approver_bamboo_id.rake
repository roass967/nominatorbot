# frozen_string_literal: true

# Temporarily script to fill in the nominee_employee_number so we can do easier lookups
# TODO: Remove this after it ran

namespace :backfill_current_approver_employee_number do
  task run: :environment do
    p "Starting the backfill."
    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    hr_client = PeopleGroup::Connectors::Bamboo.new

    Nomination.where(current_approver_employee_number: nil, status: Nomination::TODO_BY_MANAGER_STATES).find_each do |nomination|
      team_member = hr_client.get_employee_details(nomination.nominee_employee_number)

      if nomination.status == "submitted"
        manager = hr_client.fetch_manager(team_member)
      else
        manager = hr_client.fetch_second_level_manager(team_member)
      end

      if team_member.nil?
        p "Can not find team manager for nomination #{nomination.id}"
        next
      end

      nomination.update!(current_approver_employee_number: manager['employeeNumber'])
    end
    end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    p "Backfill done. It took #{end_time - start_time} seconds"
  end
end
