# frozen_string_literal: true

namespace :retrigger_idle_nominations do
  task run: :environment do
    # We can only run retriggers for nominations that have slack_ts
    # filled in. As our retrigger system uses that attribute.
    Nomination.where.not(slack_ts: nil).find_each do |nomination|
      next unless nomination.can_be_retriggered?(72)

      Review::Retrigger.call(nomination: nomination)
    end
  end
end
