# frozen_string_literal: true

require 'optparse'

namespace :offboarding do
  task :dry_run, [:employee_number] => :environment do |t, args|
    employee_number = args[:employee_number]
    return unless employee_number

    Nomination.where(current_approver_employee_number: employee_number).find_each do |nomination|
      p nomination
    end
  end

  task :run, [:employee_number] => :environment do |t, args|
    employee_number = args[:employee_number]
    return unless employee_number

    Nomination.where(current_approver_employee_number: employee_number).find_each do |nomination|
      Review::Manager.call(nomination: nomination) if nomination.submitted?
      Review::SecondLevel.call(nomination: nomination) if nomination.manager_approved?
    end
  end
end
